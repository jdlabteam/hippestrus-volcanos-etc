---
title: "Comparing WT stage-stage DEGs to prior studies"
author: "Bernie Mulvey"
date: "1/30/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11,hjust=0.5)))
```

```{r}
sprague <- fread("../../estrous hippocamp DEGs from prior studies/sprague dawley pro vs est degs iqbal tan 2019.txt")

names(sprague)
names(posthocs)
posthocs[[27]][external_gene_name %in% sprague$Pro_grthn_Est,logFC]
posthocs[[27]][external_gene_name %in% sprague$Pro_grthn_Est,adj.P.Val]
# 9 / 11 upreg in sprague pro matched; 6/9 matched dir were FDR < 0.1 (4 < 0.05)
posthocs[[27]][external_gene_name %in% sprague$Pro_lsthn_Est,logFC]
posthocs[[27]][external_gene_name %in% sprague$Pro_lsthn_Est,adj.P.Val]
# 2/6 downreg in sprague pro matched dir; 1/2 of these were FDR < 0.11 (0.004 and 0.107)
rm(sprague)
```

```{r}
msnowa <- fread("../../estrous hippocamp DEGs from prior studies/ms hippo est cycle DEGs dicarlo vied nowakowski j comparative neuro 17.txt")

names(msnowa)
corresp.hocs <- c(27,27,23,23,22,22,26,26,24,24,25,25)
# flips <- c(3,4,9,10,11,12)
corresp.dir <- c("up","down","down","up","up","down","up","down","down","up","down","up")

reptab <- as.data.table(matrix(ncol=36,nrow=0))
setnames(reptab,seq(from=1,to=34,by=3),names(msnowa))
setnames(reptab,seq(2,35,by=3),paste0(names(msnowa),"_dirmatch"))
setnames(reptab,seq(3,36,by=3),paste0(names(msnowa),"_dirmatch_andsig"))
reptab <- as.data.frame(reptab)
i<-1
for (i in seq(1,34,by=3)){
  curhoc <- corresp.hocs[(i+2)/3]
  curnowaname <- names(msnowa)[(i+2)/3]
  curdir <- corresp.dir[(i+2)/3]
  if (curdir=="down"){
    reptab[1,i] <- nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])])
    reptab[1,(i+1)] <- nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])&logFC<0])
    reptab[1,(i+2)] <-  nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])&logFC<0&adj.P.Val<0.05])
  }
  else{
    reptab[1,i] <- nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])])
    reptab[1,(i+1)] <- nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])&logFC0])
    reptab[1,(i+2)] <-  nrow(posthocs[[curhoc]][external_gene_name %in% unlist(msnowa[,..curnowaname])&logFC0&adj.P.Val<0.05])
  }
}
reptab <- as.data.table(reptab)
rm(i,curdir,curnowaname,curhoc)

msnowa.plot <- melt(reptab,value.vars=names(reptab))

i<-1
newvals <- c("PE","P<E","EM","E<M","MD","M<D","DP","D<P","PM","P<M","ED","E<D")
for (i in c(1:12)){
  if(i==1){
    pltgrp <- rep(newvals[i],3)
  }
  else{
    pltgrp <- c(pltgrp,rep(newvals[i],3))
  }
}
rm(i,newvals)

msnowa.plot[,group:=pltgrp]

msnowa.plot[,variable:=rep(c("in_data","dir_rep","dir_rep_and_FDR5"),times=12)]
rm(pltgrp)

# pdf("Replicability of stage-stage WT DEGs in Dicarlo 2017 ms hippocampus 021022.pdf",height=7,width=10)
ggplot(msnowa.plot,aes(x=factor(group,levels=c("PE","P<E","EM","E<M","MD","M<D","DP","D<P","PM","P<M","ED","E<D")),y=value,fill=factor(variable,levels=c("in_data","dir_rep","dir_rep_and_FDR5"))))+
  geom_col(position="dodge")+
  labs(fill="Matching Type")+
  xlab("Dicarlo 17 Comparison and Direction")+
  ylab("# of Genes")+
  theme(axis.text.x=element_text(angle=75,hjust=1))
dev.off()


rm(corresp.dir,corresp.hocs,msnowa,reptab,msnowa.plot)
gc(full=T)
```

Ocanas et al 22 preprint aggregated sex DE genes reported in the lit, and did a four core genotypes experiment themselves.

```{r}
union <- fread("ocanas22 hip sex DE datasets/all reported sex DE ever.txt",header=F)

unionout <- as.data.frame(matrix(nrow=length(posthocs),ncol=3))
unionout[,1] <- names(posthocs)
unionout[,2] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[adj.P.Val<0.05&external_gene_name %in% union$V1])}))
unionout[,3] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[adj.P.Val<0.05])}))


# nope thats a linear relationship for sure.
rm(union,unionout)

twoplus <- fread("ocanas22 hip sex DE datasets/ocanas22 mouse hip autosomal sex DE 2plus studies.txt",header=F)

twoplusout <- as.data.frame(matrix(nrow=length(posthocs),ncol=3))
twoplusout[,1] <- names(posthocs)
twoplusout[,2] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[adj.P.Val<0.05&external_gene_name %in% twoplus$V1])}))
twoplusout[,3] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[adj.P.Val<0.05])}))
##same
rm(twoplusout)


fcg <- fread("ocanas22 hip sex DE datasets/ocanas fcg results.txt")
names(fcg)

setnames(fcg,c(1:7),c("gene","adj.p.sex","adj.p.sexchrint","adj.p.chr","p.sex","p.sexchrint","p.chr"))
setnames(fcg,70,"symb")
fcg[1]

fcgout <- as.data.frame(matrix(nrow=length(posthocs),ncol=5))
fcgout[,1] <- names(posthocs)
fcgout[,2] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[P.Value<0.05&external_gene_name %in% fcg[p.sex<0.05,symb]])}))
fcgout[,3] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[P.Value<0.05&external_gene_name %in% fcg[p.sexchrint<0.05,symb]])}))
fcgout[,4] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[P.Value<0.05&external_gene_name %in% fcg[p.chr<0.05,symb]])}))
fcgout[,5] <- unlist(lapply(posthocs,FUN=function(x){nrow(x[P.Value<0.05])}))

fcgout <- as.data.table(fcgout)
fcgout[,ko:=0]
fcgout[V1 %in% grep(V1,pattern="KO",value=T),ko:=1]
fcgout[V1 %in% grep(V1,pattern="all",value=T),ko:=2]
fcgplot <- melt(fcgout,id.vars=c("V1","V5","ko"))
fcgplot[,variable:=as.character(variable)]
fcgplot[variable=="V2",variable:="sex"]
fcgplot[variable=="V3",variable:="sexhcrint"]
fcgplot[variable=="V4",variable:="chr"]
ggplot(fcgplot,aes(x=V5,y=value,col=variable))+geom_point()
ggplot(fcgplot,aes(x=V5,y=value,col=ko))+geom_point()+facet_wrap(.~variable)
fcgplot[,ko:=factor(ko,levels=c(0,1,2))]
ggplot(fcgplot,aes(x=V5,y=value,col=ko))+geom_point()+facet_wrap(.~variable)
dev.off() 

### nothing to see there either
rm(fcg,fcgout,fcgplot,twoplus)
gc(full=T)
```
