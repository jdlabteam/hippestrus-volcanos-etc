---
title: "Sup Tab-Shared and unique stage-stage genes between WT and KO"
author: "Bernie Mulvey"
date: "2/5/2022"
output: html_document
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11,hjust=0.5)))
```

```{r}
stagestagegenes <- lapply(posthocs[c(22:33)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})

restab <- as.data.table(as.data.frame(matrix(nrow=6,ncol=1)))
restab[,V1:=names(posthocs)[c(1:6)]]
setnames(restab,1,"stagestagecomp")
restab[,WT_ndeg:=0]
restab[,KO_ndeg:=0]
restab[,nshare:=0]
restab[,nWTuniq:=0]
restab[,nKOuniq:=0]
restab <- as.data.frame(restab)

for (i in c(1:6)){
  restab[i,2] <- length(stagestagegenes[[i]])
  restab[i,3] <- length(stagestagegenes[[(i+6)]])
  restab[i,4] <- max(c(sum(stagestagegenes[[i+6]] %in% stagestagegenes[[i]],na.rm=T),sum(stagestagegenes[[i]] %in% stagestagegenes[[i+6]],na.rm=T)))
  restab[i,5] <- sum(!(stagestagegenes[[i]] %in% stagestagegenes[[i+6]]))
  restab[i,6] <- sum(!(stagestagegenes[[i+6]] %in% stagestagegenes[[i]]))
}
rm(i)

restab <- as.data.table(restab)
restab[,stagestagecomp:=gsub(stagestagecomp,pattern="(.*)_.*$",replacement="\\1")]
# write.table(restab,"Sup tabs/Sup Table-Number of Unique and Shared FDR signif DEGs from WT stage-stage and KO stage-stage estrous comparisons 021022.txt",sep='\t',quote=F,row.names=F,col.names=T)
```