genes	joint.WT.KO.subcluster	orig.WT.clust
Wnt3	1	1
Zfp276	1	1
Ndrg1	1	1
Cadm3	1	1
Tmbim1	1	1
Gltp	1	1
Cers2	1	1
Nipal4	1	1
Itgb4	1	1
Cyp46a1	1	1
Sema4d	1	1
Sh3bp5	1	1
Creld2	1	1
Ache	1	1
Tfeb	1	1
Man2a1	1	1
Tapbp	1	1
Slc12a2	1	1
Pcsk5	1	1
Lipa	1	1
Frmd8	1	1
Itgb1	1	1
Fam20c	1	1
Efhd1	1	1
Tmem63a	1	1
Gsn	1	1
Cd82	1	1
Tyro3	1	1
Adam15	1	1
Ctsk	1	1
Slc4a2	1	1
Fkbp9	1	1
Adipor2	1	1
Plp1	1	1
Gab1	1	1
D430042O09Rik	1	1
Fa2h	1	1
Efcab14	1	1
Unc93b1	1	1
Zfp692	1	1
Ldlrap1	1	1
Cldn11	1	1
Enpp6	1	1
Slc43a2	1	1
Car14	1	1
Scamp2	1	1
Zfp598	1	1
Trir	1	1
Rnpep	1	1
Gjc2	1	1
Gjb1	1	1
Gal3st1	1	1
Kcnt1	1	1
Kif13b	1	1
Tmem151a	1	1
Arhgef10	1	1
Mog	1	1
Slc48a1	1	1
Cers1	1	1
Raf1	2	1
Retreg3	2	1
Ube2d1	2	1
Atf1	2	1
St8sia5	2	1
Tmem70	2	1
Unc50	2	1
Lrrfip1	2	1
Mpzl1	2	1
Slc2a8	2	1
Wdr5	2	1
Pdia3	2	1
Mfsd1	2	1
Sike1	2	1
Crtc2	2	1
Akr7a5	2	1
Ddost	2	1
Zfp593	2	1
Atad3a	2	1
Ccnl2	2	1
Mapre3	2	1
Dgat2	2	1
Ap1s2	2	1
Saraf	2	1
Nptn	2	1
Ift57	2	1
Gde1	2	1
Rnf43	2	1
Tbc1d10a	2	1
Pdk3	2	1
Plk5	2	1
Acaa1a	2	1
Fam53a	2	1
Fam76b	2	1
B3galnt2	2	1
Azi2	2	1
Arntl2	2	1
Zbed3	2	1
Vwa1	2	1
Pheta1	2	1
Rell1	2	1
Insc	2	1
Tmem120b	2	1
Srsf11	2	1
Pus7	2	1
Med9	2	1
Naa15	2	1
Kti12	2	1
Mir100hg	2	1
Kcng1	2	1
Srrm4os	2	1
Ube2d2a	2	1
Gm42692	2	1
C030006K11Rik	2	1
Acvr1b	3	1
Sema4f	3	1
Sema6b	3	1
Clptm1	3	1
Prkcsh	3	1
Ddr1	3	1
Efnb3	3	1
Mcoln1	3	1
Myo9b	3	1
Bcan	3	1
Elovl1	3	1
Slc4a3	3	1
Aplp1	3	1
Hapln4	3	1
Gprc5b	3	1
Tmem184b	3	1
Slc3a2	3	1
Psen2	3	1
Adamtsl4	3	1
Reep3	3	1
Map7	3	1
Srebf1	3	1
Wscd1	3	1
Zkscan3	3	1
Slc39a14	3	1
Grina	3	1
Cpox	3	1
Cers5	3	1
Enpp4	3	1
Gabbr1	3	1
Pcyox1l	3	1
Esrra	3	1
Slit1	3	1
Ogfod3	3	1
Aatk	3	1
Ptdss2	3	1
Rnpepl1	3	1
Ext2	3	1
Frmd5	3	1
Mal	3	1
Slc33a1	3	1
Ybx3	3	1
Vasp	3	1
Syt3	3	1
Porcn	3	1
Magt1	3	1
Cx3cl1	3	1
Jam3	3	1
Dscaml1	3	1
Tspan3	3	1
Atp1b3	3	1
Scap	3	1
Atxn2l	3	1
Chpf	3	1
Sox10	3	1
Lpcat2	3	1
Crlf2	3	1
Lgi3	3	1
Kcnk1	3	1
Foxj1	3	1
Scara3	3	1
Lrrn1	3	1
Tmem98	3	1
Mbd3	3	1
Mboat7	3	1
Dagla	3	1
Tmem63b	3	1
Mag	3	1
Nfkbid	3	1
Kcnh3	3	1
Phf2	3	1
Igsf8	3	1
Cep131	3	1
Srrm3	3	1
Tex264	3	1
Crtac1	3	1
Arrdc4	3	1
Irf2bp1	3	1
Megf8	3	1
Sox18	3	1
Cxxc5	3	1
Slc39a3	3	1
Selplg	3	1
Phldb1	3	1
Cdc42ep1	3	1
Slc29a4	3	1
Tmem125	3	1
Lingo3	3	1
Pcdh1	3	1
Kcnf1	3	1
Kcnn2	3	1
Tmem62	3	1
Tmem119	3	1
Pcnx3	3	1
Zfp580	3	1
Gjc3	3	1
Kcnc3	3	1
Slc35g2	3	1
Safb	3	1
Rhog	3	1
Snhg20	3	1
Tmem189	3	1
Gm27032	3	1
Gm43362	3	1
Adamts4	4	1
Cilk1	4	1
Csf1	4	1
Sema6a	4	1
Tmcc3	4	1
Rffl	4	1
Elovl7	4	1
Trim35	4	1
Dnajc3	4	1
Dgat1	4	1
Slc25a46	4	1
Tm9sf2	4	1
Atg16l1	4	1
Klhl12	4	1
Gatm	4	1
Ehd4	4	1
Serp1	4	1
Tspan2	4	1
Prpf38b	4	1
Dnajc2	4	1
Srd5a3	4	1
Pgam5	4	1
BC017158	4	1
Fam53b	4	1
Morc4	4	1
Spg21	4	1
Trf	4	1
Uba5	4	1
Mrpl3	4	1
Snx33	4	1
Prr5l	4	1
Ugt8a	4	1
Spata2l	4	1
Ttyh2	4	1
Tspan15	4	1
Clic4	4	1
Galnt6	4	1
Mapkap1	4	1
Pde12	4	1
S1pr5	4	1
Kcnk13	4	1
Gpr17	4	1
Ggcx	4	1
H2aj	4	1
Plekhh1	4	1
Hhip	4	1
Fnbp1	4	1
Mfsd14a	4	1
Sec14l5	4	1
Sox2ot	4	1
Gmpr	1	3
Hddc2	1	3
Snapin	1	3
Nudt14	1	3
Atp5d	1	3
Inmt	1	3
Dgcr6	1	3
Mtfp1	1	3
Eif2b2	1	3
Ap1s1	1	3
Ddah2	1	3
Ube2g2	1	3
Pgam1	1	3
Nat9	1	3
Gdap1l1	1	3
Isyna1	1	3
Fkbp8	1	3
Gipc1	1	3
Cfap410	1	3
Guk1	1	3
Fkbp1b	1	3
Slc9a3r1	1	3
Prelid1	1	3
Amacr	1	3
Polr3h	1	3
Sharpin	1	3
Thap7	1	3
Cfap298	1	3
Tuba1b	1	3
Celf4	1	3
Mrpl16	1	3
Vps51	1	3
Rps6ka4	1	3
Dus1l	1	3
Chmp6	1	3
Hgs	1	3
Stk25	1	3
Rab29	1	3
Pycr2	1	3
Ak1	1	3
Ndufa8	1	3
Pacsin3	1	3
Pmf1	1	3
Nans	1	3
Stoml2	1	3
Stmn1	1	3
Nudc	1	3
Fastk	1	3
Cdk5	1	3
Fbxo44	1	3
Prxl2b	1	3
Arl6ip4	1	3
Ddx54	1	3
Ccdc184	1	3
Pole4	1	3
Psmd8	1	3
Dkkl1	1	3
Mrpl17	1	3
Rrp8	1	3
Rab3a	1	3
Etfa	1	3
Cmtm7	1	3
Faim	1	3
Klhdc8b	1	3
Spr	1	3
Nubpl	1	3
Ccdc106	1	3
R3hdm4	1	3
Galt	1	3
Acaa2	1	3
Ninj1	1	3
Pcp4l1	1	3
Angptl6	1	3
Sccpdh	1	3
Shd	1	3
Ppp1ca	1	3
Oaz2	1	3
Gm17018	1	3
Nudt18	1	3
Samd14	1	3
Gap43	1	3
Cbr1	1	3
Ezr	1	3
Myh7	1	3
Hmgb2	1	3
Fbxo6	1	3
Nap1l5	1	3
Cfl1	1	3
Acbd4	1	3
Commd7	1	3
Egln2	1	3
Dhps	1	3
Mfap2	1	3
Nr1h2	1	3
Haghl	1	3
Phactr2	1	3
Nnat	1	3
Hpcal1	1	3
Ppia	1	3
BC029722	1	3
Gpx4	1	3
Smim1	1	3
Rab26	1	3
Dynlt1b	1	3
Gm36908	1	3
Arvcf	2	3
Flii	2	3
Sh3bp5l	2	3
Pdlim4	2	3
Zfyve21	2	3
Enpp2	2	3
Krt18	2	3
Trip6	2	3
Plin3	2	3
Ostf1	2	3
Ppp2r5b	2	3
Sirt7	2	3
Nfkb2	2	3
Rdh5	2	3
Ppp1r35	2	3
Hibadh	2	3
Ucp2	2	3
Ccnd3	2	3
Rhov	2	3
Card19	2	3
Dxo	2	3
Id1	2	3
Defb11	2	3
Msx1	2	3
Krt8	2	3
Eva1b	2	3
Lgals3	2	3
Serpinb1b	2	3
Rtca	3	3
Araf	3	3
Ckb	3	3
Pnck	3	3
Gtf2f1	3	3
Paf1	3	3
Syce2	3	3
Kifc2	3	3
Tbcb	3	3
Mid1ip1	3	3
Fhl2	3	3
Dmap1	3	3
Slc25a5	3	3
Uchl5	3	3
Mrpl45	3	3
Cdc37	3	3
Rab4a	3	3
Cops6	3	3
Ubxn6	3	3
Dtd2	3	3
Tbc1d7	3	3
Eci2	3	3
Psmd6	3	3
Nkiras1	3	3
Eif3h	3	3
Samm50	3	3
Pmm1	3	3
Ntan1	3	3
Atp6v1g2	3	3
Cidea	3	3
Jph3	3	3
Atp5b	3	3
Dctn2	3	3
Crcp	3	3
Atp5c1	3	3
Pnkd	3	3
Ndufa10	3	3
Emc4	3	3
Ndufaf1	3	3
Ndufaf5	3	3
Nsfl1c	3	3
Fam110a	3	3
Rbck1	3	3
Ppcs	3	3
Tmem222	3	3
Ssu72	3	3
Ube2k	3	3
Snx8	3	3
Chchd6	3	3
Gabarapl1	3	3
Ldhb	3	3
Tamm41	3	3
Crym	3	3
Glrx3	3	3
Naxd	3	3
Rpl4	3	3
Rwdd2a	3	3
Pebp1	3	3
Ephx4	3	3
Sncb	3	3
Selenof	3	3
Nudt22	3	3
Mrm3	3	3
Adrm1	3	3
Dcxr	3	3
Acp1	3	3
Tubg2	3	3
Cirbp	3	3
Zfp688	3	3
Cradd	3	3
Gm11223	3	3
Hmga1	3	3
Lxn	3	3
Tmem106c	3	3
Asna1	3	3
Dand5	3	3
Phgdh	3	3
Triqk	3	3
Sipa1	3	3
Dtnbp1	3	3
Rrp1	3	3
Cebpzos	3	3
Eef1g	3	3
Rab9	3	3
Cbx6	3	3
B230311B06Rik	3	3
Prmt1	3	3
Slc5a5	4	3
Hars	4	3
Fbxw9	4	3
Gfod2	4	3
Cacybp	4	3
Gstz1	4	3
Arg2	4	3
Fdft1	4	3
Adsl	4	3
Tpi1	4	3
Atat1	4	3
Asrgl1	4	3
Prdx3	4	3
Pcyt2	4	3
Sirt3	4	3
Mdh1b	4	3
Tuba4a	4	3
Phyh	4	3
Psmb7	4	3
Fahd2a	4	3
Idh3b	4	3
Stmn2	4	3
Pdcd10	4	3
Nudt17	4	3
Gtf2b	4	3
Scp2	4	3
Urod	4	3
Acot7	4	3
Lias	4	3
Commd8	4	3
Eif2b1	4	3
Ran	4	3
Cops7a	4	3
Arfip2	4	3
Uqcrc2	4	3
Lhpp	4	3
Nsdhl	4	3
Ciapin1	4	3
Coq9	4	3
Psmb10	4	3
Vstm5	4	3
Gnb5	4	3
Gorasp1	4	3
Ndn	4	3
Cplx1	4	3
Cdk10	4	3
Nipsnap1	4	3
Cops8	4	3
Nagk	4	3
Grhpr	4	3
Gpi1	4	3
Aaas	4	3
Tubb4b	4	3
Arhgap33	4	3
Actr1b	4	3
Camk4	4	3
Pdk2	4	3
Mettl22	4	3
Tcta	4	3
Etfrf1	4	3
Dok4	4	3
Bag2	4	3
Atpaf2	4	3
Mpp3	4	3
Sae1	4	3
Mllt11	4	3
Aldh7a1	4	3
Ccdc28a	4	3
Cend1	4	3
Agbl4	4	3
Akr1b10	4	3
Tubb3	4	3
Tubb4a	4	3
Chia1	4	3
Actg1	4	3
Scoc	4	3
Pepd	4	3
Gng3	4	3
Tuba1a	4	3
Ass1	4	3
Rab11b	4	3
Arpc4	4	3
Poldip2	5	3
Med25	5	3
Impdh1	5	3
Map3k11	5	3
Ppan	5	3
Apba3	5	3
Vps72	5	3
Akt1s1	5	3
Med16	5	3
Qdpr	5	3
Fam241b	5	3
Sf3a2	5	3
Hmg20b	5	3
Galk1	5	3
Sh3bp1	5	3
Pcbp4	5	3
Bin1	5	3
Cdk2ap2	5	3
Rab1b	5	3
Grb14	5	3
Mapk8ip1	5	3
Col9a3	5	3
Lypla2	5	3
Pex14	5	3
Stx18	5	3
Dnajb6	5	3
Camkk2	5	3
Bcl7b	5	3
Mvb12a	5	3
Pik3r2	5	3
Bcar1	5	3
Mon1a	5	3
Gga1	5	3
Cactin	5	3
Josd2	5	3
Shfl	5	3
Npepl1	5	3
Wdr34	5	3
Eif4h	5	3
Rnf208	5	3
Nyap1	5	3
Fbl	5	3
Zfp219	5	3
Ogfr	5	3
Ccdc137	5	3
Tmem198	5	3
Ankrd24	5	3
Ppp1r1b	5	3
Chadl	5	3
Hes6	5	3
Sf3b4	5	3
Spint2	5	3
Tox2	5	3
Lcmt2	5	3
Mrpl10	6	3
Puf60	6	3
Prkra	6	3
Ppp5c	6	3
As3mt	6	3
Nqo1	6	3
Tax1bp1	6	3
Slbp	6	3
Aamp	6	3
Zfp184	6	3
Stx1a	6	3
Cct4	6	3
Pea15a	6	3
Slc25a11	6	3
Dstn	6	3
Actr8	6	3
Atxn10	6	3
Glod4	6	3
Tnfaip1	6	3
Cyb5r3	6	3
Cdk5rap3	6	3
Cops3	6	3
Gtf3c6	6	3
Ppa1	6	3
Tbata	6	3
Mdh1	6	3
Pfn4	6	3
Vdac2	6	3
Sap18	6	3
Sucla2	6	3
Mapk8ip2	6	3
Trmt2a	6	3
Cndp2	6	3
Kat5	6	3
Psmd13	6	3
Asl	6	3
Clybl	6	3
Hprt	6	3
Uqcrc1	6	3
Lancl1	6	3
Dph7	6	3
Mapre1	6	3
Tpd52	6	3
Arfgap1	6	3
Ufm1	6	3
Polr1e	6	3
Rpa2	6	3
Nrbp1	6	3
Glmn	6	3
Cct6a	6	3
Tfr2	6	3
Ruvbl1	6	3
Abtb1	6	3
Bckdk	6	3
Aktip	6	3
Acat1	6	3
Fbxo22	6	3
Apeh	6	3
Heyl	6	3
Psmf1	6	3
Wbp2	6	3
Gcsh	6	3
Cog7	6	3
Aldh1b1	6	3
Cep19	6	3
Cenpt	6	3
Apbb1	6	3
Eef1a1	6	3
Ndufv1	6	3
Smarce1	6	3
Rlbp1	6	3
Ptpa	6	3
Septin6	6	3
Nsmce2	6	3
Flot1	6	3
Suclg2	6	3
Eno1	6	3
Ppp1r3c	6	3
Ttc30b	6	3
Maea	6	3
Gm11549	6	3
Snrpn	6	3
Lsm2	7	3
Slc25a39	7	3
Rdh14	7	3
Auh	7	3
Sorbs3	7	3
Rbfa	7	3
Eps8l2	7	3
Mff	7	3
Capn10	7	3
Rbm38	7	3
Fscn1	7	3
Malsu1	7	3
Zyx	7	3
Strap	7	3
Fkbp4	7	3
Rab20	7	3
Klhl36	7	3
Cog8	7	3
Dbndd1	7	3
Mpv17l2	7	3
Tsen34	7	3
Cavin3	7	3
Ppp1r14a	7	3
Clba1	7	3
Cdc16	7	3
Uqcrfs1	7	3
Gfer	7	3
Adprhl2	7	3
Gemin7	7	3
Fam43a	7	3
Cfl2	7	3
Eid2b	7	3
Ccdc85b	7	3
