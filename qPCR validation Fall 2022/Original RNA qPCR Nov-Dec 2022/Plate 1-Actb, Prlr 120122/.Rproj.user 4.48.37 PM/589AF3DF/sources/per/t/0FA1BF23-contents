---
title: "Repeated measures ANOVA"
author: "Bernie Mulvey"
date: "2022-12-05"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)
#### sets tab autocompletion for directories to begin from the directory containing the .Rproj session, instead of the script's directory if different
knitr::opts_knit$set(root.dir = here::here())

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)

theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11,hjust=0.5)))
```

### Comparisons of interest: KO_Proest vs KO_Male; WT_Metest vs WT_Proest; WT_Proest v WT_Est, KO Metest vs KO Proest 
```{r}
cts <- fread("single well cts actb prlr.txt")
idmap <- fread("../welllut.txt")

cts[,rna_well:=gsub(rna_well,pattern="^(.)(.)$",replacement=paste0("\\1","0","\\2"))]

cts1 <- unique(cts[targ=="Actb",.(rna_well,mean_ct)])
setnames(cts1,2,"mean_actb_ct")
cts2 <- unique(cts[targ=="Prlr",.(rna_well,targ,ct)])
cts <- merge.data.table(cts1,cts2,by="rna_well")
cts <- merge.data.table(cts,idmap,by.x="rna_well",by.y="RNAwell")
rm(cts1,cts2)


cts[,well_dct:=ct-mean_actb_ct]
library(rstatix)
# KO_Proest vs KO_Male; WT_Metest vs WT_Proest; WT_Proest v WT_Est, KO Metest vs KO Proest 
KP_Kmale <- copy(cts[altgroup %in% c("KO_P","KO_male")])
WM_WP <- copy(cts[altgroup %in% c("WT_M","WT_P")])
WP_WE <- copy(cts[altgroup %in% c("WT_E","WT_P")])
KM_KP <- copy(cts[altgroup %in% c("KO_M","KO_P")])

KP_Kmale_res <- anova_test(data = KP_Kmale,dv = well_dct,wid = sample,between = altgroup,detailed=T)
WM_WP_res <- anova_test(data = WM_WP,dv = well_dct,wid = sample,between = altgroup,detailed=T)
WP_WE_res <- anova_test(data = WP_WE,dv = well_dct,wid = sample,between = altgroup,detailed=T)
KM_KP_res <- anova_test(data = KM_KP,dv = well_dct,wid = sample,between = altgroup,detailed=T)

restab <- rbind(KP_Kmale_res,WM_WP_res,WP_WE_res,KM_KP_res)
restab <- as.data.table(restab)
restab[,comparison:=c("KO_P_vs_KO_male","WT_Met_vs_WT_Pro","WT_Pro_vs_WT_Est","KO_Met_vs_KO_Pro")]

# write.table(restab,"Repeated Measures ANOVA of pairwise conditions of interest 120522.txt",sep='\t',quote=F,row.names=F,col.names=T)

rm(list=ls())
gc(full=T)
```