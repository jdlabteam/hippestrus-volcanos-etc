---
title: Get post-hoc contrasts between WT estrus stages and male, WT and KO estrus
  stages 010622
author: "Bernie Mulvey"
date: "1/6/2022"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)
require(limma)
require(edgeR)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11)))
```

```{r}
iso2 <- readRDS("Master ISOFORM expression dataset for plotting, etc with estrous restaged 031022.RDS")
isog <- iso[c(11:15,20)]
names(isog[[1]])

isog[[6]][external_gene_name=="Cnih"]
iso[[2]][external_gene_name=="Cnih3"]
i<-1
for(i in c(1:33)){
  if(i==1){
    res <- iso[[i]][ensembl_gene_id=="ENSMUSG00000026514"]}
  else{
    res <- rbind(res,iso[[i]][ensembl_gene_id=="ENSMUSG00000026514"])
  }
}
```

```{r}
meta <- fread("../meta_restaged_021022.txt")
meta[,geno:=as.factor(geno)]
meta[,geno:=relevel(geno,ref="WT")]
meta[,estrus:=as.factor(estrus)]
meta[,estrus:=relevel(estrus,ref="male")]
meta <- as.data.frame(meta)
rownames(meta) <- meta[,1]

counts <- readRDS("../../counts matrix as data frame, genes in order expresso2 was using 01-06-22.RDS")
stopifnot(colnames(counts)==rownames(meta))

genes <- fread("../../Estrus MGI 121021/all.gene_counts.tsv")
genes <- unique(genes[ensembl_gene_id %in% rownames(counts),c(1,3:7)])
genes <- as.data.frame(genes)
rownames(genes) <- genes$ensembl_gene_id
genes <- genes[rownames(counts),]
stopifnot(sum(rownames(genes)==rownames(counts))==nrow(counts))

dge <- DGEList(counts=counts,samples = meta,genes = genes)
dge <- calcNormFactors.DGEList(dge,"TMM")

mod <- model.matrix(~0+altgroup,data=dge$samples)
colnames(mod) <- gsub(colnames(mod),pattern="^altgroup(.*)$",replacement="\\1")

trasts <- makeContrasts(
  MvMale=WT_M-WT_male,
  DvMale=WT_D-WT_male,
  EvMale=WT_E-WT_male,
  PvMale=WT_P-WT_male,
  KOMvMale=KO_M-KO_male,
  KODvMale=KO_D-KO_male,
  KOEvMale=KO_E-KO_male,
  KOPvMale=KO_P-KO_male,
  MvF=(0.25*WT_M+0.25*WT_D+0.25*WT_P+0.25*WT_E)-WT_male,
  KOMvF=(0.25*KO_M+0.25*KO_D+0.25*KO_P+0.25*KO_E)-KO_male,
  MaleGeno=KO_male-WT_male,
  MGeno=KO_M-WT_M,
  DGeno=KO_D-WT_D,
  EGeno=KO_E-WT_E,
  PGeno=KO_P-WT_P,
  MvMall=(KO_M+WT_M)-(KO_male+WT_male),
  DvMall=(KO_D+WT_D)-(KO_male+WT_male),
  EvMall=(KO_E+WT_E)-(KO_male+WT_male),
  PvMall=(KO_P+WT_P)-(KO_male+WT_male),
  sexall=(0.25*KO_M+0.25*KO_D+0.25*KO_P+0.25*KO_E+0.25*WT_M+0.25*WT_D+0.25*WT_P+0.25*WT_E)-(KO_male+WT_male),
  genoall=(0.2*KO_M+0.2*KO_D+0.2*KO_P+0.2*KO_E+0.2*KO_male)-(0.2*WT_M+0.2*WT_D+0.2*WT_P+0.2*WT_E+0.2*WT_male),
  MvD_WT=WT_M-WT_D,
  MvE_WT=WT_M-WT_E,
  MvP_WT=WT_M-WT_P,
  DvE_WT=WT_D-WT_E,
  DvP_WT=WT_D-WT_P,
  PvE_WT=WT_P-WT_E,
  MvD_KO=KO_M-KO_D,
  MvE_KO=KO_M-KO_E,
  MvP_KO=KO_M-KO_P,
  DvE_KO=KO_D-KO_E,
  DvP_KO=KO_D-KO_P,
  PvE_KO=KO_P-KO_E,
  levels=mod)

dge.qw <- voomWithQualityWeights(dge,mod)
# masterxp <- as.data.table(copy(dge.qw$E),keep.rownames=T)
# masterxp <- merge.data.table(masterxp,genes,by.x="rn",by.y="ensembl_gene_id")
# names(masterxp)
# masterxp <- masterxp[,c(57:61,2:56)]
# saveRDS(masterxp,"Master expression dataset for plotting, etc with estrous restaged 021022.RDS")
# rm(masterxp)
dge.fit <- lmFit(dge.qw,design=mod)
dge.fit1 <- contrasts.fit(dge.fit,contrasts = trasts)

MvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvMale",number = Inf,adjust.method = "BH"))
DvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvMale",number = Inf,adjust.method = "BH"))
EvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "EvMale",number = Inf,adjust.method = "BH"))
PvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "PvMale",number = Inf,adjust.method = "BH"))

KOMvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "KOMvMale",number = Inf,adjust.method = "BH"))
KODvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "KODvMale",number = Inf,adjust.method = "BH"))
KOEvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "KOEvMale",number = Inf,adjust.method = "BH"))
KOPvMale <- as.data.table(topTable(eBayes(dge.fit1),coef = "KOPvMale",number = Inf,adjust.method = "BH"))

MvF <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvF",number = Inf,adjust.method = "BH"))
KOMvF <- as.data.table(topTable(eBayes(dge.fit1),coef = "KOMvF",number = Inf,adjust.method = "BH"))

MaleGeno <- as.data.table(topTable(eBayes(dge.fit1),coef = "MaleGeno",number = Inf,adjust.method = "BH"))
MGeno <- as.data.table(topTable(eBayes(dge.fit1),coef = "MGeno",number = Inf,adjust.method = "BH"))
DGeno <- as.data.table(topTable(eBayes(dge.fit1),coef = "DGeno",number = Inf,adjust.method = "BH"))
EGeno <- as.data.table(topTable(eBayes(dge.fit1),coef = "EGeno",number = Inf,adjust.method = "BH"))
PGeno <- as.data.table(topTable(eBayes(dge.fit1),coef = "PGeno",number = Inf,adjust.method = "BH"))

MvMall <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvMall",number = Inf,adjust.method = "BH"))
DvMall <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvMall",number = Inf,adjust.method = "BH"))
EvMall <- as.data.table(topTable(eBayes(dge.fit1),coef = "EvMall",number = Inf,adjust.method = "BH"))
PvMall <- as.data.table(topTable(eBayes(dge.fit1),coef = "PvMall",number = Inf,adjust.method = "BH"))


sexall <- as.data.table(topTable(eBayes(dge.fit1),coef = "sexall",number = Inf,adjust.method = "BH"))

genoall <- as.data.table(topTable(eBayes(dge.fit1),coef = "genoall",number = Inf,adjust.method = "BH"))


MvD_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvD_WT",number = Inf,adjust.method = "BH"))

MvE_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvE_WT",number = Inf,adjust.method = "BH"))

MvP_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvP_WT",number = Inf,adjust.method = "BH"))

DvE_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvE_WT",number = Inf,adjust.method = "BH"))

DvP_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvP_WT",number = Inf,adjust.method = "BH"))

PvE_WT <- as.data.table(topTable(eBayes(dge.fit1),coef = "PvE_WT",number = Inf,adjust.method = "BH"))


MvD_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvD_KO",number = Inf,adjust.method = "BH"))

MvE_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvE_KO",number = Inf,adjust.method = "BH"))

MvP_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "MvP_KO",number = Inf,adjust.method = "BH"))

DvE_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvE_KO",number = Inf,adjust.method = "BH"))

DvP_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "DvP_KO",number = Inf,adjust.method = "BH"))

PvE_KO <- as.data.table(topTable(eBayes(dge.fit1),coef = "PvE_KO",number = Inf,adjust.method = "BH"))


nrow(MvMale[adj.P.Val<0.05])
nrow(DvMale[adj.P.Val<0.05])
nrow(EvMale[adj.P.Val<0.05])
nrow(PvMale[adj.P.Val<0.05])

nrow(MaleGeno[adj.P.Val<0.05])
nrow(MGeno[adj.P.Val<0.05])
nrow(DGeno[adj.P.Val<0.05])
nrow(EGeno[adj.P.Val<0.05])
nrow(PGeno[adj.P.Val<0.05])



posthocs <- list(MvMale,DvMale,EvMale,PvMale,KOMvMale,KODvMale,KOEvMale,KOPvMale,MvF,KOMvF,MaleGeno,MGeno,DGeno,EGeno,PGeno,MvMall,DvMall,EvMall,PvMall,sexall,genoall,MvD_WT, MvE_WT, MvP_WT, DvE_WT, DvP_WT, PvE_WT,MvD_KO, MvE_KO, MvP_KO, DvE_KO, DvP_KO, PvE_KO)
names(posthocs) <- c("MvMale","DvMale","EvMale","PvMale","KOMvMale","KODvMale","KOEvMale","KOPvMale","MvF","KOMvF","MaleGeno","MGeno","DGeno","EGeno","PGeno","MvMall","DvMall","EvMall","PvMall","sexall","genoall","MvD_WT", "MvE_WT", "MvP_WT", "DvE_WT", "DvP_WT", "PvE_WT","MvD_KO", "MvE_KO", "MvP_KO", "DvE_KO", "DvP_KO", "PvE_KO")

```

```{r}
# saveRDS(posthocs,"Posthoc DE Tables-2x4 geno-spec Estrus Stage vs WT Male, 5x KO vs WT Estrus Stage (or male), 2x geno-spec F v M, 4x geno-agnostic estrus vs male, geno-agnostic sex, sex-agnostic geno, named list of topTables est restaged 021022.RDS")

rm(list=ls())
gc(full=T)
```

```{r}
meta <- fread("../meta_restaged_021022.txt")
meta[,geno:=as.factor(geno)]
meta[,geno:=relevel(geno,ref="WT")]
meta[,estrus:=as.factor(estrus)]
meta[,estrus:=relevel(estrus,ref="male")]
meta <- as.data.frame(meta)
rownames(meta) <- meta[,1]

xscrips <- fread("analyzed_transcript_someparticular_results_justhereforgeneidsandorder.tsv")

keep <- xscrips[,.N,by="Feature_ID"][N==1,Feature_ID]

rm(xscrips)

xcounts <- fread("../Estrus MGI 121021/all.transcript_counts.tsv")
# alt analysis including Cnih3:
# keep <- c(keep,unique(xcounts[external_gene_name=="Cnih3",ensembl_transcript_id]))
xcounts <- xcounts[ensembl_transcript_id %in% keep]
names(xcounts)

genes <- as.data.frame(xcounts[,c(1:7)])
rownames(genes) <- genes$ensembl_transcript_id

xcountskeepn <- c("ensembl_transcript_id",meta[,1])
xcounts <- xcounts[,..xcountskeepn]
xcounts <- as.data.frame(xcounts)
rownames(xcounts) <- xcounts$ensembl_transcript_id

sum(rownames(xcounts)==rownames(genes))
xcounts$ensembl_transcript_id <- NULL
sum(colnames(xcounts)==rownames(meta))

xdge <- DGEList(counts=xcounts,samples = meta,genes = genes)
xdge <- calcNormFactors.DGEList(xdge,"TMM")

mod <- model.matrix(~0+altgroup,data=xdge$samples)
colnames(mod) <- gsub(colnames(mod),pattern="^altgroup(.*)$",replacement="\\1")

trasts <- makeContrasts(
  MvMale=WT_M-WT_male,
  DvMale=WT_D-WT_male,
  EvMale=WT_E-WT_male,
  PvMale=WT_P-WT_male,
  KOMvMale=KO_M-KO_male,
  KODvMale=KO_D-KO_male,
  KOEvMale=KO_E-KO_male,
  KOPvMale=KO_P-KO_male,
  MvF=(0.25*WT_M+0.25*WT_D+0.25*WT_P+0.25*WT_E)-WT_male,
  KOMvF=(0.25*KO_M+0.25*KO_D+0.25*KO_P+0.25*KO_E)-KO_male,
  MaleGeno=KO_male-WT_male,
  MGeno=KO_M-WT_M,
  xdgeno=KO_D-WT_D,
  EGeno=KO_E-WT_E,
  PGeno=KO_P-WT_P,
  MvMall=(KO_M+WT_M)-(KO_male+WT_male),
  DvMall=(KO_D+WT_D)-(KO_male+WT_male),
  EvMall=(KO_E+WT_E)-(KO_male+WT_male),
  PvMall=(KO_P+WT_P)-(KO_male+WT_male),
  sexall=(0.25*KO_M+0.25*KO_D+0.25*KO_P+0.25*KO_E+0.25*WT_M+0.25*WT_D+0.25*WT_P+0.25*WT_E)-(KO_male+WT_male),
  genoall=(0.2*KO_M+0.2*KO_D+0.2*KO_P+0.2*KO_E+0.2*KO_male)-(0.2*WT_M+0.2*WT_D+0.2*WT_P+0.2*WT_E+0.2*WT_male),
  MvD_WT=WT_M-WT_D,
  MvE_WT=WT_M-WT_E,
  MvP_WT=WT_M-WT_P,
  DvE_WT=WT_D-WT_E,
  DvP_WT=WT_D-WT_P,
  PvE_WT=WT_P-WT_E,
  MvD_KO=KO_M-KO_D,
  MvE_KO=KO_M-KO_E,
  MvP_KO=KO_M-KO_P,
  DvE_KO=KO_D-KO_E,
  DvP_KO=KO_D-KO_P,
  PvE_KO=KO_P-KO_E,
  levels=mod)

xdge.qw <- voomWithQualityWeights(xdge,mod)
# xmasterxp <- as.data.table(copy(xdge.qw$E),keep.rownames=T)
# xmasterxp <- merge.data.table(xmasterxp,genes,by.x="rn",by.y="ensembl_transcript_id")
# names(masterxp)
# xmasterxp <- xmasterxp[,c(57:62,2:56)]
# saveRDS(xmasterxp,"Master ISOFORM expression dataset for plotting, etc with estrous restaged 031022.RDS")


### save separately for the alternative set with cnih3 added back despite not passing filter explicitly:
# cnih3isoxp <- as.data.table(copy(xdge.qw$E),keep.rownames=T)
## cnih3isoxp <- merge.data.table(cnih3isoxp,genes,by.x="rn",by.y="ensembl_transcript_id")
# names(cnih3isoxp)
# cnih3isoxp <- cnih3isoxp[external_gene_name=="Cnih3",c(57:62,2:56)]
# saveRDS(cnih3isoxp,"cnih3 isoform expression (after manually adding isoforms back into data despite filterout) dataset for plotting, etc with estrous restaged 040422.RDS")
# rm(xmasterxp)
xdge.fit <- lmFit(xdge.qw,design=mod)
xdge.fit1 <- contrasts.fit(xdge.fit,contrasts = trasts)

MvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvMale",number = Inf,adjust.method = "BH"))
DvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvMale",number = Inf,adjust.method = "BH"))
EvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "EvMale",number = Inf,adjust.method = "BH"))
PvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "PvMale",number = Inf,adjust.method = "BH"))

KOMvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "KOMvMale",number = Inf,adjust.method = "BH"))
KODvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "KODvMale",number = Inf,adjust.method = "BH"))
KOEvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "KOEvMale",number = Inf,adjust.method = "BH"))
KOPvMale <- as.data.table(topTable(eBayes(xdge.fit1),coef = "KOPvMale",number = Inf,adjust.method = "BH"))

MvF <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvF",number = Inf,adjust.method = "BH"))
KOMvF <- as.data.table(topTable(eBayes(xdge.fit1),coef = "KOMvF",number = Inf,adjust.method = "BH"))

MaleGeno <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MaleGeno",number = Inf,adjust.method = "BH"))
MGeno <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MGeno",number = Inf,adjust.method = "BH"))
xdgeno <- as.data.table(topTable(eBayes(xdge.fit1),coef = "xdgeno",number = Inf,adjust.method = "BH"))
EGeno <- as.data.table(topTable(eBayes(xdge.fit1),coef = "EGeno",number = Inf,adjust.method = "BH"))
PGeno <- as.data.table(topTable(eBayes(xdge.fit1),coef = "PGeno",number = Inf,adjust.method = "BH"))

MvMall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvMall",number = Inf,adjust.method = "BH"))
DvMall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvMall",number = Inf,adjust.method = "BH"))
EvMall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "EvMall",number = Inf,adjust.method = "BH"))
PvMall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "PvMall",number = Inf,adjust.method = "BH"))


sexall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "sexall",number = Inf,adjust.method = "BH"))

genoall <- as.data.table(topTable(eBayes(xdge.fit1),coef = "genoall",number = Inf,adjust.method = "BH"))


MvD_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvD_WT",number = Inf,adjust.method = "BH"))

MvE_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvE_WT",number = Inf,adjust.method = "BH"))

MvP_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvP_WT",number = Inf,adjust.method = "BH"))

DvE_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvE_WT",number = Inf,adjust.method = "BH"))

DvP_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvP_WT",number = Inf,adjust.method = "BH"))

PvE_WT <- as.data.table(topTable(eBayes(xdge.fit1),coef = "PvE_WT",number = Inf,adjust.method = "BH"))


MvD_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvD_KO",number = Inf,adjust.method = "BH"))

MvE_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvE_KO",number = Inf,adjust.method = "BH"))

MvP_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "MvP_KO",number = Inf,adjust.method = "BH"))

DvE_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvE_KO",number = Inf,adjust.method = "BH"))

DvP_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "DvP_KO",number = Inf,adjust.method = "BH"))

PvE_KO <- as.data.table(topTable(eBayes(xdge.fit1),coef = "PvE_KO",number = Inf,adjust.method = "BH"))


nrow(MvMale[adj.P.Val<0.05])
nrow(DvMale[adj.P.Val<0.05])
nrow(EvMale[adj.P.Val<0.05])
nrow(PvMale[adj.P.Val<0.05])

nrow(MaleGeno[adj.P.Val<0.05])
nrow(MGeno[adj.P.Val<0.05])
nrow(xdgeno[adj.P.Val<0.05])
nrow(EGeno[adj.P.Val<0.05])
nrow(PGeno[adj.P.Val<0.05])



xposthocs <- list(MvMale,DvMale,EvMale,PvMale,KOMvMale,KODvMale,KOEvMale,KOPvMale,MvF,KOMvF,MaleGeno,MGeno,xdgeno,EGeno,PGeno,MvMall,DvMall,EvMall,PvMall,sexall,genoall,MvD_WT, MvE_WT, MvP_WT, DvE_WT, DvP_WT, PvE_WT,MvD_KO, MvE_KO, MvP_KO, DvE_KO, DvP_KO, PvE_KO)
names(xposthocs) <- c("MvMale","DvMale","EvMale","PvMale","KOMvMale","KODvMale","KOEvMale","KOPvMale","MvF","KOMvF","MaleGeno","MGeno","DGeno","EGeno","PGeno","MvMall","DvMall","EvMall","PvMall","sexall","genoall","MvD_WT", "MvE_WT", "MvP_WT", "DvE_WT", "DvP_WT", "PvE_WT","MvD_KO", "MvE_KO", "MvP_KO", "DvE_KO", "DvP_KO", "PvE_KO")

saveRDS(xposthocs,"ISOFORM De posthocs stack with restaged estrous 031022.RDS")
rm(list=ls())
gc(full=T)
```

```{r}
xpost <- readRDS("ISOFORM De posthocs stack with restaged estrous 031022.RDS")

keep <- xpost[[1]][,.N,by="ensembl_gene_id"][N>1,ensembl_gene_id]

xpost2 <- lapply(xpost,FUN=function(x){
  tmp <- copy(x[ensembl_gene_id %in% keep&adj.P.Val<0.1,ensembl_gene_id])
  y <- x[ensembl_gene_id %in% tmp]
})


saveRDS(xpost2,"ISOFORM DE contrast stack, subsetted to genes with 2+ iso expressed and 1+ DE at fdr 0.1 in that contrast 031022.RDS")
rm(list=ls())
gc(full=T)
```
