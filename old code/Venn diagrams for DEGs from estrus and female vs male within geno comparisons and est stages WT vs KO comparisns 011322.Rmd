---
title: "Venn diagrams for DEGs"
author: "Bernie Mulvey"
date: "1/13/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)
require(ggrepel)
require(ggvenn)
require(ggVennDiagram)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11)))
```

### Est male color scheme: ("#F8766D","#7CAE00","#00BFC4","#C77CFF")
### ("Metestrus", "Diestrus", "Estrus", "Proestrus")

### JOE -- the VennDiagram package uses base plotting for all of its formatting etc, so these will be easy for you to fiddle with as you desire down the road. can save files as svg, tiff, or png. (guessing only svg comes away illustrator-able.)
```{r}
posthocs <- readRDS("../Posthoc contrasts-estrus stages vs male, geno-wise comparisons 010622/Posthoc DE Tables-2x4 geno-spec Estrus Stage vs WT Male, 5x KO vs WT Estrus Stage (or male), 2x geno-spec F v M, 4x geno-agnostic estrus vs male, geno-agnostic sex, sex-agnostic geno, 6x2 geno-spec stage vs stage, named list of topTables 020422.RDS")

cols <- c("#F8766D","#7CAE00","#00BFC4","#C77CFF")

names(posthocs)

wtvenn <- list(posthocs[[1]][adj.P.Val<0.05,external_gene_name],posthocs[[2]][adj.P.Val<0.05,external_gene_name],posthocs[[3]][adj.P.Val<0.05,external_gene_name],posthocs[[4]][adj.P.Val<0.05,external_gene_name])
names(wtvenn) <- names(posthocs)[c(1:4)]

kovenn <- lapply(posthocs[c(5:8)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})
names(kovenn) <- names(posthocs)[c(5:8)]

# pdf("venns/wt_eststage-v-male_020622.pdf",height=5,width=5)
ggvenn(data = wtvenn,fill_color = cols,fill_alpha = 0.4,show_percentage = F)
dev.off()

# pdf("venns/ko_eststage-v-male_020622.pdf",height=5,width=5)
ggvenn(data=kovenn,fill_color=cols,fill_alpha=0.4,show_percentage = F)
dev.off()

rm(kovenn,wtvenn)
# venn.diagram(x=wtvenn,fill=cols,filename="venns/wt_eststage-v-male_011322.tiff",imagetype = "tiff",output=F,category.names = c("Met vs. Male","Di vs. Male","Est vs. Male","Pro vs. Male"),disable.logging = T,main = "Estrus Stage vs Male-WT")

# venn.diagram(x=kovenn,fill=cols,filename="venns/ko_eststage-v-male_011322.tiff",imagetype = "tiff",output=F,category.names = c("Met vs. Male","Di vs. Male","Est vs. Male","Pro vs. Male"),disable.logging = T,main = "Estrus Stage vs Male-Cnih3 KO")

cols5 <- c(cols[c(1)],"lightgray",cols[c(3:4)])

# d: no genes
genovenn <- lapply(posthocs[c(12,11,15,14)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})
names(genovenn) <- names(posthocs)[c(12,11,14,15)]

# pdf("genotype comparisons venn 020622.pdf",height=6,width=6)
ggvenn(data=genovenn,fill_color = cols5,fill_alpha=0.4,show_percentage = F)
dev.off()
rm(genovenn)
# venn.diagram(x=genovenn,fill=cols5,filename="venns/withinstage_KOvsWT_011322.tiff",imagetype="tiff",output=F,category.names=c("Met","Di","Male","Est","Pro"),disable.logging=T,main="Cnih3 KO vs WT within Estrous Stage")

# wtsexvsstage <- lapply(posthocs[c(22,24:27)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})
# names(wtsexvsstage) <- names(posthocs)[c(22,24:27)]
# 
# ggvenn(wtsexvsstage,columns = c(1:5))

# venn.diagram(x=wtsexvsstage,fill=cols5,filename="venns/WT_estrusvsmale_and_FvM_011322.svg",filetype="svg",output=F,category.names=c("Met vs. Male","Di vs. Male","Female vs. Male","Est vs. Male","Pro vs. Male"),disable.logging=T,main="Estrus or Female vs Male-WT")


# venn.diagram(x=kosexvsstage,fill=cols5,filename="venns/KO_estrusvsmale_and_FvM_011322.tiff",filetype="tiff",output=F,category.names=c("Met vs. Male","Di vs. Male","Female vs. Male","Est vs. Male","Pro vs. Male"),disable.logging=T,main="Estrus or Female vs Male-KO")
rm(cols,cols2,cols5,wtvenn,wtsexvsstage,kosexvsstage,kovenn,sexvenn,genovenn)
gc(full=T)
```

#### WT stage-stage comparison venns for FDR signif genes. MvE=0 so not shown. Can't do it with ggvenn actuallly since there's 5 comparisons with genes here. but can do it for KO (MvE and PvE both 0)
```{r}
cols5 <- c(cols,"lightgray")
stagestagevenn <- lapply(posthocs[c(22,24:27)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})
names(stagestagevenn) <- names(posthocs)[c(22,24:27)]
stagestagevenn <- Venn(stagestagevenn)
stagestagevenn <- process_data(stagestagevenn)

pdf("venns/WT stage stage 5way venn AIable 020622.pdf",height=6,width=6)
ggplot()+
geom_sf(data = venn_region(stagestagevenn))+
  geom_sf(color = "black", data = venn_setedge(stagestagevenn), show.legend = FALSE)+
  geom_sf_label(aes(label = count), data = venn_region(stagestagevenn))+
  geom_sf(aes(fill=id), data = venn_region(stagestagevenn),show.legend = FALSE,alpha=0.2)+
  geom_sf_text(aes(label = name), data = venn_setlabel(stagestagevenn),inherit.aes = F,alpha=1)+
  theme_void()
dev.off()

# venn.diagram(x=stagestagevenn,filename="venns/WT_stage-stageDEGs_FDR05_venn test.tiff",filetype="pdf",category.names=c("Met vs Di","Met vs Pro","Di vs Est","Di vs Pro","Pro vs Est"),fill=cols5,disable.logging=T,main="WT Estrous Stage-\nwise DEGs (FDR<0.05)")
dev.off()
rm(stagestagevenn,cols5,cols)

kostagestage <- lapply(posthocs[c(28,30:32)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})

names(kostagestage) <- names(posthocs)[c(28,30:32)]
# pdf("KO stage stage DEG venn 020622.pdf",height=5,width=5)
ggvenn(kostagestage,fill_color = cols,fill_alpha = 0.4,show_percentage = F)
dev.off()
```

```{r}
rm(cols,cols5,stagestagevenn,kostagestage,wtsexvsstage)
```