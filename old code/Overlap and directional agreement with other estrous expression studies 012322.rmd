---
title: "Overlap and directional agreement with other estrous expression studies 012322"
author: "Bernie Mulvey"
date: "1/23/2022"
output: html_document
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
require(gridExtra)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11,hjust=0.5)))
```

#### set 1, knoedler 22 cell: 4-(non hippocampal) region est vs diest, male vs each esr1+ TRAP 
```{r}
posthocs <- readRDS("../Posthoc contrasts-estrus stages vs male, geno-wise comparisons 010622/Posthoc DE Tables-2x4 geno-spec Estrus Stage vs WT Male, 5x KO vs WT Estrus Stage (or male), 2x geno-spec F v M, 4x geno-agnostic estrus vs male, geno-agnostic sex, sex-agnostic geno, named list of topTables 013122.RDS")
kned <- fread("../allDEGcontrasts from esr1 trap knoedler inoue 22 cell.txt")

### how many genes overlap here at FDR 5%
length(unique(kned[compare %in% c("MaleVDi","MaleVEst"),gene]))
# 2096 unique male-vs-stage degs there, 17 shared with WT, 646 shared if also considering Cnih3 contrasts 
unique(kned[compare %in% c("MaleVDi","MaleVEst")&gene %in% unique(unlist(lapply(posthocs[c(2,3)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]}))),gene])
unique(kned[compare %in% c("MaleVDi","MaleVEst")&gene %in% unique(unlist(lapply(posthocs[c(2,3,6,7)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]}))),gene])

## how many genes overlap for est-diest comparison?
length(unique(kned[compare=="EstVDi",gene]))
## 862; 195 in WT est-di comparison (302 if also including the KO version of contrast)
unique(kned[compare=="EstVDi"&gene %in% unique(unlist(lapply(posthocs[c(25)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]}))),gene])
unique(kned[compare=="EstVDi"&gene %in% unique(unlist(lapply(posthocs[c(25,31)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]}))),gene])

### join in hippest lfcs

kned.malestage.compare.nom <- kned[gene %in% unique(unlist(lapply(posthocs[c(2,3,6,7,25,31)],FUN=function(x){x[P.Value<0.05,external_gene_name]})))]
kned.malestage.compare.sig <- kned[gene %in% unique(unlist(lapply(posthocs[c(2,3,6,7,25,31)],FUN=function(x){x[adj.P.Val<0.05,external_gene_name]})))]

k <- 2
newnames <- c("lfc_wtDvMale","lfc_wtEvMale","lfc_koDvMale","lfc_koEvMale","lfc_wtDvE","lfc_koDvE")
for (k in c(2,3,6,7,25,31)){
  i <- which((c(2,3,6,7,25,31))==k)
  kned.malestage.compare.nom <- merge.data.table(kned.malestage.compare.nom,posthocs[[k]][P.Value<0.05,.(external_gene_name,logFC)],by.x="gene",by.y="external_gene_name",all.x=T)
  setnames(kned.malestage.compare.nom,"logFC",newnames[i])
  kned.malestage.compare.sig <- merge.data.table(kned.malestage.compare.sig,posthocs[[k]][adj.P.Val<0.05,.(external_gene_name,logFC)],by.x="gene",by.y="external_gene_name",all.x=T)
  setnames(kned.malestage.compare.sig,"logFC",newnames[i])
}
rm(newnames,i,k)


names(kned.malestage.compare.nom)==names(kned.malestage.compare.sig)

### reverse LFC directions to match knoedler contrasts
kned.malestage.compare.nom[,lfc_wtDvMale:=-lfc_wtDvMale]
kned.malestage.compare.nom[,lfc_koDvMale:=-lfc_koDvMale]
kned.malestage.compare.nom[,lfc_wtEvMale:=-lfc_wtEvMale]
kned.malestage.compare.nom[,lfc_koEvMale:=-lfc_koEvMale]
kned.malestage.compare.nom[,lfc_wtDvE:=-lfc_wtDvE]
kned.malestage.compare.nom[,lfc_koDvE:=-lfc_koDvE]

kned.malestage.compare.sig[,lfc_wtDvMale:=-lfc_wtDvMale]
kned.malestage.compare.sig[,lfc_koDvMale:=-lfc_koDvMale]
kned.malestage.compare.sig[,lfc_wtEvMale:=-lfc_wtEvMale]
kned.malestage.compare.sig[,lfc_koEvMale:=-lfc_koEvMale]
kned.malestage.compare.sig[,lfc_wtDvE:=-lfc_wtDvE]
kned.malestage.compare.sig[,lfc_koDvE:=-lfc_koDvE]

overlaps.agreements.tab <- as.data.table(as.data.frame(matrix(nrow=6,ncol=1)))
overlaps.agreements.tab[,inknoedset:=0]
overlaps.agreements.tab[,inhipestsetFDR05:=0]
overlaps.agreements.tab[,inhipestsetnom:=0]
overlaps.agreements.tab[,sharedfdr:=0]
overlaps.agreements.tab[,sharednom:=0]
overlaps.agreements.tab[,diragreefdr:=0]
overlaps.agreements.tab[,diragreenom:=0]
setnames(overlaps.agreements.tab,1,"hipest_contrast")
overlaps.agreements.tab[,hipest_contrast:=names(kned.malestage.compare.nom)[c(7:12)]]

overlaps.agreements.tab <- as.data.frame(overlaps.agreements.tab)

i <- 1
j <- c(2,3,6,7,25,31)

for (i in c(1:6)){
  curcond <- names(kned.malestage.compare.nom)[i+6]
  if (i %in% c(1,3)){
    curnom <- kned.malestage.compare.nom[compare == "MaleVDi"]
    cursig <- kned.malestage.compare.sig[compare=="MaleVDi"]
    overlaps.agreements.tab[i,2] <- length(unique(kned[compare=="MaleVDi",gene]))
  }
  else if (i %in% c(2,4)){
    curnom <- kned.malestage.compare.nom[compare == "MaleVEst"]
    cursig <- kned.malestage.compare.sig[compare=="MaleVEst"]
    overlaps.agreements.tab[i,2] <- length(unique(kned[compare=="MaleVEst",gene]))
  }
  else {
    curnom <- kned.malestage.compare.nom[compare=="EstVDi"]
    cursig <- kned.malestage.compare.sig[compare=="EstVDi"]
    overlaps.agreements.tab[i,2] <- length(unique(kned[compare=="EstVDi",gene]))
  }
  overlaps.agreements.tab[i,3] <- nrow(posthocs[[j[i]]][adj.P.Val<0.05])
  overlaps.agreements.tab[i,4] <- nrow(posthocs[[j[i]]][P.Value<0.05])
  overlaps.agreements.tab[i,5] <- nrow(cursig[!is.na(get(curcond))])
  overlaps.agreements.tab[i,6] <- nrow(curnom[!is.na(get(curcond))])
  overlaps.agreements.tab[i,7] <- nrow(cursig[!is.na(get(curcond))&get(curcond)*lfc>0])
  overlaps.agreements.tab[i,8] <- nrow(curnom[!is.na(get(curcond))&get(curcond)*lfc>0])
  rm(curnom,cursig)
}
rm(i,j,curcond,kned.malestage.compare.nom,kned.malestage.compare.sig,kned)
# write.table(overlaps.agreements.tab,"Shared DEGs and directionality agreement with fdr 5 esr1 trap combined MvEst MvDi EstvDi poa bnst vmhvl mea from Knoedler 22 with corresp WT and KO contrasts in this data 012322.txt",sep='\t',quote=F,row.names=F,col.names=T)
```
